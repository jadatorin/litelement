import {
  LitElement,
  html,
  css,
} from "https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js";

export class MyCounter extends LitElement {
  static styles = [
    css`
      :host {
        display: block;
      }
    `,
  ];
  static get properties() {
    return {
      counter: { type: Number },
    };
  }
  constructor() {
    super();
    this.counter = 0;
  }
  render() {
    return html`
      <style>
        div {
          border: 1px solid #ddd;
        }
        .x {
          background-color: greenyellow;
        }
      </style>
      <div class="x">Llevas ${this.counter} clicks</div>
      <div>
        <button @click="${this.incrementar}">+1</button>
        <button @click="${this.decrementar}">-1</button>
      </div>
    `;
  }
  incrementar() {
    this.counter++;
  }
  decrementar() {
    this.counter--;
  }
}
customElements.define("my-counter", MyCounter);
