import {
  LitElement,
  html,
  css,
} from "https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js";

export class HolaMundo extends LitElement {
  static styles = css`
    p {
      color: blue;
    }
  `;
  static properties = {
    name: { type: String },
  };
  constructor() {
    super();
    this.name = "Somebody";
    console.log("hola");
  }

  render() {
    return html`<p>${this.name}</p>`;
  }
}
customElements.define("hola-mundo", HolaMundo);
